def print_permutations(string):
    arr = list(string)
    print_permutations_helper(arr, 0)


def print_permutations_helper(arr, i):
    if i == (len(arr)-1):
        print(''.join(arr))
    for j in range(i, len(arr)):
        swap(arr, i, j)
        print_permutations_helper(arr, i + 1)
        swap(arr, i, j)


def print_parity_permutations(arr):
    print_parity_permutations_helper(arr, 0, True)


def print_parity_permutations_helper(arr, i, last_parity):
    target_parity = not last_parity
    if i == (len(arr)-1):
        print(arr)
    for j in range(i, len(arr)):
        element = arr[j]
        element_parity = is_even(element)
        if element_parity == target_parity:
            swap(arr, i, j)
            print_parity_permutations_helper(arr, i + 1, element_parity)
            swap(arr, i, j)
        else:
            pass


def swap(array, a_index, b_index):
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp


def is_even(a):
    if a % 2 == 0:
        return True
    else:
        return False


def print_anagarams(string):
    char_array = list(string)
    print_anagrams_helper(char_array, 0)


def print_anagrams_helper(arr, i):
    if i == (len(arr)-1) and valid_word(arr):
        print(''.join(arr))
    for j in range(i, len(arr)):
        swap(arr, i, j)
        if valid_prefix(arr, i + 1):
            print_anagrams_helper(arr, i + 1)
        swap(arr, i, j)


def valid_word(char_array):
    string = ''.join(char_array)
    word_dict = ['least', 'setal', 'slate', 'stale', 'steal', 'stela', 'taels', 'tales', 'teals', 'tesla']
    if string in word_dict:
        return True
    else:
        return False


def valid_prefix(char_array, current_char_index):
    string = ''.join(char_array[:current_char_index])
    word_dict = ['least', 'setal', 'slate', 'stale', 'steal', 'stela', 'taels', 'tales', 'teals', 'tesla']
    prefix_dict = []
    for i in range(0, len(word_dict)):
        prefix_dict.append(word_dict[i][:current_char_index])
    if string in prefix_dict:
        return True
    else:
        return False


arr1 = list("least")
print_anagarams(arr1)