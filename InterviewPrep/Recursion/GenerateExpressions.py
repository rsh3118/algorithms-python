from __future__ import print_function

"""
Step 0:

Get to know the problem

This problem is a little too complicated to even think of examples for
but basically a couple observations we can make about the problem is that
there are three possible choices of operation: join, add, and multiply

We also know if there are n numeric characters in the string we have exactly
n - 1 operations we will do on those characters

Another observation we can make about the problem is that no matter what operation we
do we will only increase the value of the final expression

say we have some expression so far x and we have to choose what to do next

if we join we will have increased the last value by an order of magnitude of at least
ten because we know some number must be there at the end of the expression and that numbers
magnitude will be increase and because we have only done at most + and * operations increasing that
number can only increase the value of the expression

If we add we are adding something to expression's value and that strictly increases it as well

If we multiply we are essentially multiplying the last evaluated expression (need to use some
form of order of operations for this) by a positive factor so the expression can only increase

Because of these facts we know that if at any point if we have part of the expression determined (call it
expression prefix) and part of it left to determine( call if remaining expression) the final expressions value is
strictly greater than the prefix value, so we know that no matter what operations we choose for it from this point
forward it is impossible to make it equal to the target value if it is already greater than or equal to ir

Step 1: Define a Recursive Solution

So if we think about this problem one idea is to have a function that simply lets prints the possible expressions
given a prefixed expression that equal the target value

This function essentially could widen the prefix by guessing all the operations it could do and then pass it along
as the same problem again

Something critical to the problem is making sure we stop making recursive calls if making the change to the expression
prefix will cause the prefix expression value to exceed or are equal to the expression value

We know that we need to print if the target value is equal to the expression value and we have done n -1 operations

So in more strict terms lets now define the contract of the recursive function

The job of the recursive function is to (given a prefix) print all possible expressions that are equivalent to the
target value

So obviously it will need the operations chosen so far (we can send this as array of n-1 with a index indicating what
the last picked operation was

We will also need to send it the original array but because we have the operations array we do not need to mutate it

Evaluating the expression can be done in three steps because we only have three operations

First we can get the number (this takes care of the join operation and it has to go first because join has the greatest
precedence)

Second (now we need to figure out order of operations for a sequence of numbers with + and * separating them) one
strategy is to use a stack where we add numbers to the stack  and empty the stack if the operations is over or is a +.

The reasoning behind this is that we know if something is in the stack and then it hits a plus its simply (whatever
number i.e. evaluated expression came before it can only be added to the rest of the expression value and the only thing
we need to (stop to evaluate the operation is for is when we have a sequence of * ) and we can just evaluate these until
the next +  come then there are like a regular number and since we are waiting for stars we dont even need a stack
because we are constantly multiplying the previous value until we hit a plus so we can just maintain a variable to do
this and another variable to mainting the accumulated expression

Step 2: Pseudocode

generate_expressions(s, target):
    initialize empty operations array of size s.size - 2
    generate_expressions_helper(s, target, op_arr, 0)

generate_expressions_helper(s, target, op_arr, operation_index):
    choose join as the operation at operation index
    evaluate if join creates a prefix that is equal to or greater than target
    if its not then call generate_expressions_helper(s, target, op_arr, operation_index + 1)

    choose add as the operation at the operation index
    evaluate if add creates a prefix that is equal to or greater than target
    if its not then call generate_expressions_helper(s, target, op_arr, operation_index + 1)

    choose multiply as the operation at the operation index
    evaluate if multiply creates a prefix that is equal to or greater than target
    if its not then call generate_expressions_helper(s, target, op_arr, operation_index + 1)

evaluate_expression(string, operations, operation_index):
    initialize the product_expression to 1 (because if u multiply even  just one number by 1 itll remain itself so its
    safe (multiplicative identity prop)

    initialize a temporary operation index that starts at zero because we start from the first operation

    initialize the sum_expression to 0 (because if we add even just one number itll remain itself - add. id. prop)

    initialize the string_index to zero because we start reading the string from index zero.

    while the string index is equal to or less than the operation index (bc we now we are using the characters up to
    the operation index + 1 od the string so if its greater than at any point we know we have reached the end of the
    expression we can evalutate

        We need to extract the next number and update the string index so we don't recount the numerics used to create
        the next number and update the operation indexes so we dont recount those join operations

        (next_number, updated_string_index, updated_operation_index) =
        get_next_number(string, temp_string_index, temp_operation_index, string_index)

        (now we are guranteed that we are either at operation_index or that the next operation is a + or a *)

        if the next operation is a plus we want to (flush the stack) i.e add whats ever in the product_expression to
        the sum expression and then set the product_expression to 1

        if the next operation is a * or nothing we want to (add it to the stack) i.e. multiply the current product
        expression by the next number (its safe cus we have just added and are the end then the product expression is
        simply the last number then we can just add it * 1 which is itself to the sum_expression at the end)

    flush the stakc (add the product_expression to the sum_expression (we will never add an extraneous one because the
    last number always becomes the last product expression if its only a number not an actually multiplicative series of
    numbers

    return the sum expression

get_next_number(string, temp_string_index, temp_operation_index, string_index):

    initialize a temporary string index starting at zero because we read the expression from left to right

    initialize a temporary operation index starting at zero because we read the operations from left to right

    next_digit = to_digit(string, temp_string_index)

    initialize the next num to the next_digit because it will be the first digit of the next num

    incerement the temp string index by 1 because you have retrieved the first digit

    while current operation is join and the temp string index is less than or equal to string index

        increment the temp operation index by one

        next_digit = toDigit(string, string_index)

        increment the temp string index by one

        (signifies adding a digit which means moving over the current digit(s) by one place and the new one)

        next_num = next_num*10 + next_digit


    return the (next num, temp_string_index, temp operation index)

to_digit(string, string_index):

    numeric = string[string_index]

    create an array where the index maps to the char for the index's char representation

    go through the array until you find the index at which the numeric is equal to the char

    return the index

Step 3: Code

"""


def generate_expressions(s, target):
    op_arr = [None] * (len(s) - 1)
    array = []
    return generate_expressions_helper(s, target, op_arr, 0, array)



def generate_expressions_helper(s, target, op_arr, operation_index, array):
    if operation_index == len(op_arr) and evaluate_expression(s, op_arr, operation_index) == target:
        array.append(add_expression(s, op_arr))
        return array

    if operation_index == len(op_arr):
        #print_expression(s, op_arr)
        return array

    op_arr[operation_index] = ""
    if evaluate_expression(s, op_arr, operation_index + 1) <= target:
        #print("1")
        array = generate_expressions_helper(s, target, op_arr, operation_index + 1, array)

    op_arr[operation_index] = "+"
    if evaluate_expression(s, op_arr, operation_index + 1) <= target:
        array = generate_expressions_helper(s, target, op_arr, operation_index + 1, array)

    op_arr[operation_index] = "*"
    if evaluate_expression(s, op_arr, operation_index + 1) <= target:
        array = generate_expressions_helper(s, target, op_arr, operation_index + 1, array)

    return array


def evaluate_expression(string, operations, operation_index):
    product_expression = 1

    temp_op_index = 0

    sum_expression = 0

    string_index = 0

    while string_index <= operation_index :

        (next_num, string_index, temp_op_index) = get_next_number(string, operations, temp_op_index, string_index)


        # (now we are guranteed that we are either at operation_index or that the next operation is a + or a *)

        # if the next operation is a plus we want to (flush the stack) i.e add whats ever in the product_expression to
        # the sum expression and then set the product_expression to 1

        product_expression *= next_num

        if temp_op_index >= len(operations):
            break

        if operations[temp_op_index] == '+':

            sum_expression += product_expression

            product_expression = 1

        temp_op_index += 1

        # if the next operation is a * or nothing we want to (add it to the stack) i.e. multiply the current product
        # expression by the next number (its safe cus we have just added and are the end then the product expression is
        # simply the last number then we can just add it * 1 which is itself to the sum_expression at the end)



    # flush the stakc (add the product_expression to the sum_expression (we will never add an extraneous one because the
    # last number always becomes the last product expression if its only a number not an actually multiplicative series
    # of numbers

    sum_expression += product_expression

    return sum_expression


def get_next_number(string, operations, temp_operation_index, string_index):

    # initialize a temporary string index starting at zero because we read the expression from left to right

    temp_string_index = string_index

    # initialize a temporary operation index starting at zero because we read the operations from left to right

    next_digit = to_digit(string, temp_string_index)

    # initialize the next num to the next_digit because it will be the first digit of the next num

    next_num = next_digit

    # incerement the temp string index by 1 because you have retrieved the first digit

    string_index += 1

    # while current operation is join and the temp string index is less than or equal to string index
    while temp_operation_index < len(operations) and operations[temp_operation_index] == "" and string_index < len(string):

        # increment the temp operation index by one

        temp_operation_index += 1

        next_digit = to_digit(string, string_index)

        # increment the temp string index by one

        string_index += 1

        # (signifies adding a digit which means moving over the current digit(s) by one place and the new one)

        next_num = next_num*10 + next_digit

    return next_num, string_index, temp_operation_index


def to_digit(string, string_index):

    numeric = string[string_index]

    # create an array where the index maps to the char for the index's char representation

    nums = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    index = -1

    for index in range(0, len(nums)):
        if numeric == nums[index]:
            break

    return index


def add_expression(s, op_array):
    string_index = 0
    operation_index = 0
    string = ''
    string += s[string_index]
    string_index += 1
    while string_index < len(s):
        string += op_array[operation_index]
        operation_index += 1
        string += s[string_index]
        string_index += 1
    return string


s = "12425654"

print(generate_expressions("8765942351445", 8765))


# 1*2+4 2*5 6+5*4




