class Tower:
    stack = [];

    def __init__(self, array):
        for item in array:
            self.add(item)

    def add(self, item):
        if len(self.stack) > 1:
            if self.stack[-1] <= item:
                raise ValueError("Breaking God's Rule: Cannot put a block heavier than the top block on the tower")
        self.stack.append(item)

towerA = Tower([4,3,2,1]);

"""
So lets start with thinking of our base case: 

    
    
    If we have towers A,B,C where B is the destination tower. We know that the last move has to 
    be moving the largest block from A to an empty B. We know that B has to be empty because there
    no other way that we could have put the last block on B if another block was on it because the
    last block is the heaviest and wud crush it. So we know that A can only have the last block on
    it because the heaviest block needs to be at the top so we can move it and nothing can be under
    the heaviest block. So we know all the remaining blocks have to be on C in order with heaviest 
    block on bottom and lightest on top. 
    
    A:
    B: n
    C: (n-1) .... 1
    
    Ok so at this point we know that we cna put any block on B because it can handle it because no 
    block will be heavier than the one currently on the bottom. We also know we can put any block on
    A because we know its empty. So lets define two cases. Case 1 is we put the next movable block
    (which we know is the lightest block) on A and Case 2 is we put the next movable block on B.
        Case 1:
        A: 1 
        B: n  
        C: (n-1) .. 2
        
        
        We now have two options we can either move the next block off C or move the block on A to B.
        Lets call Case 1.1 that we move the next block off C and Case 1.2 that we move the block on A
        to B.
            Case 1.1:
            A: 1
            B: n 2
            C: (n-1) ... 3
            
            We know that if we had to move a block of C it could only go to B. However now we can no
            longer move anything off of C because both towers A and B have lighter blocks on the top.
            So our only option is to move the block off A to B.
            
            A: 
            B: n 2 1
            C: (n-1) ... 3
            
            Now we have two options either move the block off B to A -- however this wud just get right
            back to where we were in the last step, so this move is not helpful, or we can move the block
            off C to A.
            
            A: 3
            B: n 2 1
            C: (n-1) ... 4
            
            Now we have two options again we can either move a block off B to A or we can move a block off 
            move the block of B to C. If we choose to move the block of B to A then the next move we cannot
            do anything but move the block off A to B (which is equivalent to undoing what we just did or 
            we can move the block off A to C which is equivalent to if we in this step had done just that.
            Thus the only viable option here is to move the block off B to C.
            
            A: 3 
            B: n 2
            C: (n-1) ... 4 1
            
            Now all we can do to not encounter one of the cases we did before is to move the block off B to A
            
            A: 3 2
            B: n 
            C: (n-1) ... 4 1
            
            
            
            
            
"""

print(towerA.stack);