def mutable_combinations(arr):
    mutable_combinations_helper(arr, 0, len(arr))


def mutable_combinations_helper(arr, i, subset_size):
    if subset_size == 0:
        return
    mutable_print_combo(arr)
    for j in range(i, len(arr)):
        temp = remove(arr, j)
        mutable_combinations_helper(arr, j + 1, subset_size - 1)
        add(arr, j, temp)


def immutable_combinations(arr):
    subset_so_far = [None] * len(arr)
    immutable_combinations_helper(arr, 0, subset_so_far, 0)


def immutable_combinations_helper(arr, i, subset_so_far, j):



    if i == len(arr):
        if j != 0:
            immutable_print_combo(subset_so_far, j)
        return
    """
    General Idea here is to split it up into two cases
        with the current element we are looking at
        without the current element we are looking at
        
    
    :param arr: 
    :param i: 
    :param subset_so_far: 
    :param j: 
    :return: 
    """
    immutable_combinations_helper(arr, i + 1, subset_so_far, j)

    subset_so_far[j] = arr[i]
    immutable_combinations_helper(arr, i + 1, subset_so_far, j + 1)


def mutable_print_combo(arr):
    for element in arr:
        if element is not None:
            print element,
    print

def immutable_print_combo(arr, i):
    for i in range(0, i):
        print arr[i],
    print


def remove(arr, index):
    temp = arr[index]
    arr[index] = None
    return temp


def add(arr, index, temp):
    arr[index] = temp


def immutable_set_sum_combinations(arr, s):
    subset_so_far = [None] * len(arr)
    immutable_set_sum_combinations_helper(arr, 0, subset_so_far, 0, s, 0)


def immutable_set_sum_combinations_helper(arr, i, subset_so_far, j, s, current_sum):
    if current_sum > s:
        return
    if s == current_sum:
        immutable_print_combo(subset_so_far, j)
        return
    if i == len(arr):
        return
    """
    General Idea here is to split it up into two cases
        with the current element we are looking at
        without the current element we are looking at


    :param arr: 
    :param i: 
    :param subset_so_far: 
    :param j: 
    :return: 
    """
    immutable_set_sum_combinations_helper(arr, i + 1, subset_so_far, j, s, current_sum)
    current_sum += arr[i]
    subset_so_far[j] = arr[i]
    immutable_set_sum_combinations_helper(arr, i + 1, subset_so_far, j + 1, s, current_sum)


arr1 = list("abc")
num_array = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16]
immutable_set_sum_combinations(num_array, 16)

