# Complete the function below.


def find_all_well_formed_brackets(n):
    # intialize all brackets as brackets to open because we cant close any without having any open
    combos = []
    find_brackets(n, 0, [], combos)
    return combos

def find_brackets(open_brackets, close_brackets, prefix, combos):

    # if we can open a bracket try that decision and add the combos that can be made given that decision
    if open_brackets > 0:
        prefix.append('[')
        find_brackets(open_brackets - 1, close_brackets + 1, prefix, combos)
        del prefix[-1]

    # if we can close a bracket try that decision and add the combos that can be made given that decision
    if close_brackets > 0:
        prefix.append(']')
        find_brackets(open_brackets, close_brackets - 1, prefix, combos)
        del prefix[-1]

    if open_brackets == 0 and close_brackets == 0:
        combos.append("".join(prefix))

    return

print(find_all_well_formed_brackets(3))