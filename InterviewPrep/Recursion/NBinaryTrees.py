"""
Step 0: Get to know the problem

We know we have n nodes to form the tree

so we can think about it kinda of like

having to always pick the root first

then having to pick left or right  with what we have left.

lets try 4

root left left left
root left left right
root left right left
root left right right
root right left left
root right left right
root right right left
root right right right

here we know its obvious for every node other than the first we have two choices
so we know just to check ourselves that using recursion we should have 2^(n-1) + 1
nodes for every n

Step 1: Think of Recursive Method

We know that we have to do it so we can see left or right as a decision we have to make with the node

Any remaining part of a binary tree is also tree

The contract for the recursive function can be given n nodes find the total number of trees the recursive function
can then delegate that decision to another function seeing how mnay trees can be made on its left and right and then
returning the total

Bases Cases:

if we get one node it has to be the root and we have no decisions to make so we should just return 1

if the recursive call gets zero nodes then we know it is the end of the tree so we can return 1 so that tree is counted
in the total

Step 2: Pseudocode
"""
def how_many_BSTs(n):
    # If there is only one we know its root and we dont have to deal with it as a special case
    binary_tree = [False] * (2 ** n -1)
    # Fill in the root
    binary_tree[0] = True
    n -= 1
    return how_many_BSTs_helper(n, 0, binary_tree, [0,1,2], 0)


def how_many_BSTs_helper(n, current_index, binary_tree, possible_nodes, binary_tree_index):

    if n == 0:
        print_bt(binary_tree)
        binary_tree[possible_nodes[current_index]] = False
        return 1


    total_trees = 0
    for current_index in range(current_index + 1, len(possible_nodes)):
        binary_tree_index = possible_nodes[current_index]
        binary_tree[binary_tree_index] = True
        n -= 1
        possible_nodes.append(2 * binary_tree_index + 1)
        possible_nodes.append(2 * binary_tree_index + 2)
        total_trees += how_many_BSTs_helper(n, current_index, binary_tree, possible_nodes, possible_nodes[current_index])
        n += 1
        del possible_nodes[-1]
        del possible_nodes[-1]
        binary_tree[possible_nodes[current_index]] = False



    return total_trees



def print_bt(arr):
    for index in range(0, len(arr)):
        if arr[index] == True:
            print(str(index) + ":" + str(True))
    print





print(how_many_BSTs(3))

"""
root 
root -> left
root -> left -> left 
root -> left -> left -> left

root 
root -> left 
root -> left -> left 
root -> left -> left -> right 

root 
root-> left 
root -> left -> left
root -> left -> right

;;;
root 
root -> right
root -> right -> left 
root -> right -> left -> left

root 
root -> right 
root -> right -> left 
root -> right -> left -> right 

root 
root-> right 
root -> right -> left
root -> right -> right

;;;







"""