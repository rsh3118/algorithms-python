# Complete the function below.


def check_if_sum_possible(arr, k):
    return check_if_sum_possible_helper(arr, 0, k, False)


def check_if_sum_possible_helper(arr, i, k, not_empty):

    if k == 0 and not_empty:
        return True

    if i == len(arr):
        return False

    # check if it is possible to make a sumset if you include the number at the index
    if check_if_sum_possible_helper(arr, i+ 1, k - arr[i], True):
        return True

    # check if it is possible to make a sumset if you exclude the number at the index
    if check_if_sum_possible_helper(arr, i + 1, k, not_empty):
        return True

    return False


arr = [2, -5, -10]
target = -15

print(check_if_sum_possible(arr, target))