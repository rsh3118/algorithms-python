"""
Step 0: Get to know the problem

u cant place n queens on a board smaller than a 4 by 4

so lets start with the 4 by 4

  -  -  Q  -
  Q  -  -  -
  -  -  -  Q
  -  Q  -  -

  (0, 0) (0, 1) (0, 2) (0, 3)
  (1, 0) (1, 1) (1, 2) (1, 3)
  (2, 0) (2, 1) (2, 2) (2, 3)
  (3, 0) (3, 1) (3, 2) (3, 3)

So the general process was guess a Queen position see if it works then try adding the next Queen

We can check if two positions are in the same left to right diagonal if they have the same col - row value

We can check if two positions are in the same right to left diagonal if they have the same col + row value


If it doesnt work backtrack up to the last decision where you had a choice and try that one

We also know that we can model the board as an array of ints because we know that each Queen needs her own column
and the row is mapped by the value at the column array index



Step 1: Recursive Solution

So we know every time we run the code we are trying to see whether or not the recursive solution works
So it make senses we should return True if the given the decisions made on the board so far if there is a possible
arrangement of N Queens

so basically the contract for our recursive helper needs to be given the board so far (given a board array with i
indicating what column we have to fill) find out it a valid Queen
arrangement exits and print it out

The recursive helper should make a decision (i.e choose a row for the column in question) and then call itseld again if
the decision was valid if it was not it should try another decision if it can and if it has no decisions it can make it
should return False

Step 0.5: Helper Function Ideas

We can have an array of columns (true, false) array to see which have been taken up (this array is of size n)

We can have another array of left right diagonals (true, false) to see which left right diagonals have been taken up
we know this array has to be 2n - 1 in size cus there are 2n -1 diagonals for any n by n board. We can reference the
diagonals in this array by (col - row ) + (n - 1) because we know doing (cal - row) will give us the left right
diagonals referenced by ( - (n - 1) ..... (n - 1) ) so we can shift these indices over in order to have the first
diagonal be at the 0th column

We can have another array of right left diagonals (true, false) to see which right left diagonals have been taken up
we know this array has to be 2n-1 size. We can reference the diagonals in this array by col + row

We can build out these arrays as we go so checking becomes an O(1) processes as we simply have to check three arrays
by index value ( the column , left right diagonal , and right left diagonal id)

We are essentially creating hash maps to do O(1) checks

Step 2: Pseudo code

"""

def n_queens(n):
    # initialize columns array
    c_arr = [None] * n
    # initialize rows array
    r_arr = [False] * n
    # initialize left right diagonals array
    lr_arr = [False] * (2*n -1)
    # initialize right left diagonals array
    rl_arr = [False] * (2*n -1)
    n_queens_helper(c_arr, 0, r_arr, lr_arr, rl_arr)
    print_board(c_arr)

def n_queens_helper(c_arr, c_index, r_arr, lr_arr, rl_arr):
    # if c_index is equal to length of c_arr which is the same as the number of rows we know we have exceed the last
    # possible column because (c -1 )th column is filled in this case if we could get so far it means that
    return
    # iterate through every possible row position

        # check if row position is valid

        # if row position is valid make recursive call

    #if you get here it means nothing worked so return false

def print_board(board):
    string = ""
    for row_number in board:
        for row in range(0, len(board)):
            if row == row_number:
                string += "  Q  "
            else:
                string += "  -  "
        string += "\n"
    print(string)









