def count_paths(grid):
    grid_depth = len(grid[0]) - 1
    grid_length = len(grid) - 1
    return count_paths_helper(grid_depth, grid_length, grid_length, grid_depth, 0, 0, 0, 0, 0)


def count_paths_helper(grid_depth, grid_length, des_x, des_y, cur_x, cur_y, right_moves, down_moves, total_paths):
    if right_moves > grid_length or down_moves > grid_depth:
        return total_paths

    if cur_x == des_x and cur_y == des_y:
        return total_paths + 1

    total_paths = count_paths_helper(
        grid_depth,
        grid_length,
        des_x,
        des_y,
        cur_x + 1,
        cur_y,
        right_moves + 1,
        down_moves,
        total_paths
    )

    total_paths = count_paths_helper(
        grid_depth,
        grid_length,
        des_x,
        des_y,
        cur_x,
        cur_y + 1,
        right_moves,
        down_moves + 1,
        total_paths
    )

    return total_paths


x_array = [None] * 3
grid = [x_array] * 3
print(count_paths(grid))
