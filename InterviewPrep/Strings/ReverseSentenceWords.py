#
# Complete the reverse_ordering_of_words function below.
#
def reverse_ordering_of_words(s):
    #
    # Write your code here.
    #
    carr = list(s)
    reverse_word(carr, 0, len(carr))
    i = 0
    while i < len(carr):
        j = i
        while j < len(carr) and carr[j] != ' ':
            j += 1
        reverse_word(carr, i, j)
        j += 1
        i = j
    return ''.join(carr)


def reverse_word(carr, start_index, end_index):
    i = 0
    end_index -= 1
    while i <= (end_index - start_index)/2:
        temp = carr[start_index + i]
        carr[start_index + i] = carr[end_index - i]
        carr[end_index - i] = temp
        i += 1


print(reverse_ordering_of_words("word1 word2ncjdncjdjncdj mkdmckd,mck,.."))
