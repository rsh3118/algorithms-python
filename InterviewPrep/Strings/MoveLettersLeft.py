#
# Complete the move_letters_to_left_side_with_minimizing_memory_writes function below.
#
def move_letters_to_left_side_with_minimizing_memory_writes(s):
    #
    # Write your code here.
    #
    carr = list(s)
    i = 0
    char_i = 0
    while i < len(carr):
        if is_alpha(carr[i]):
            carr[char_i] = carr[i]
            char_i += 1
        i += 1
    return ''.join(carr)


def is_alpha(character):
    if ord(character) >= 65 and ord(character) <= 90:
        return True
    if ord(character) >= 97 and ord(character) <= 122:
        return True

print(move_letters_to_left_side_with_minimizing_memory_writes("0a193zbr"))
