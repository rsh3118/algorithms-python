"""
Step 0: Get to know the problem

if we have

1 1 1 1
1 1 0 1
1 1 1 1
"""
def numberOfPaths(a):
    # Determine size of DP table
    table = [[-1 for x in range(len(a[0]))] for y in range(len(a))]

    # Initialize table
    print_table(table)
    for col in range(0, len(a[0])):
        if a[len(a) - 1][col] != 0:
            table[len(a) - 1][col] = 1
        else:
            table[len(a) - 1][col] = 0

    for row in range(0, len(a)):
        if a[row][len(a[0]) - 1] != 0:
            table[row][len(a[0]) - 1] = 1
        else:
            table[row][len(a[0]) - 1] = 0

    print("init")

    print_table(table)

    for row in range(len(a) - 2, -1 , -1):
        for col in range(len(a[0]) - 2, -1, -1):
            if a[row][col] != 0:
                table[row][col] = table[row + 1][col] + table[row][col + 1]
            else:
                table[row][col] = 0
            print_table(table)

    print_table(table)
    return table[0][0]

def print_table(table):
    print("_____________________________________________")
    for row in table:
        print(row)
    print("_____________________________________________")



# Complete the numberOfPaths function below.
a = [[1,1,1,1], [1,1,0,1], [1,1,1,1]]

print(numberOfPaths(a))
