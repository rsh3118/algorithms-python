"""
Step 0:

Get to know the problem

say we have the word cat and the word bird

cat and bird

we we can either insert, delete, or replace at every index

If we think about inserting, deleting, or replacing the actual text in a the source string or some kind of temporary
string the problem can become quite complex as we would have the deal with the runtime cost of doing some of those
operations, which can be quite heavy, and doing it that way may actually may tougher to check if we have a solution as
well as knowing where in the array to make the change

Another way to think of the problem is to split each string into two parts ( a part that is matched and a part that
is unmatched) and we can only deal with the matching the parts of the string that are unmatched. We know that the
matched part and unmatched parts should be contiguous. If the matched areas are spread throughout the unmatched area
than we really haven't reduced the size of the problem. So one method is starting with our matching set to be zero for
both strings so if we want our index to denote something about the match and we know the lowest index is zero we can say
all indexes below the current index are matched (for the empty set with both the source and destination index being zero
we now there are no indices under zero -- which means no elements have been matched, so we know modeling the index as
being the point at with both strings stop being matched is valid)

So lets get into the operations. We know if the char values at the two indices are the same then we can add these to the
matched group so we can just increment our indices (this is basically our noop). If this isn't the case we know that the
chars at those indices are different but out goal to grow the size of the matched array has not changed so we know we
need to do something to change the array. First off we can modify so we would increment our cost with the cost of modify
and then increment both indices because we know now that they are the same after modification.

//// ASIDE //////////////////


It is tempting here to
think because the modify operation is the same cost as both the delete and the insert operations that we should just
modify all that we can and then if we have extra on the source just delete and extra on the destination just modify.
Here is a sample case showing why that does not work:

bigbart

biigbart

here if we started modifying at the second i and then we would have to modify every character after it. When we couldve
just inserted one i and not had to do anything else. In simple cases like this its easy to see that insert was the
obvious operation that we needed to choose in order to minimize the transformation cost but its not always so simple to
see how one decision made much earlier in the array could impact the total cost and thats why we must try every possible
choice we could make at an unmatched index in order to catch all the possible solutions.

////////////////////////////

The next thing we can try is deleting. Before we get into how we will model the delete operation lets think about what
we are actually doing by deleting. Deleting essentially removes the element from the unmatched set of the source string,
and decreases the size of the number of elements left potentially unmatched (all the elements we haven't encountered yet
- elements from the [index + 1: end]. If we want decrease the number of elements unmatched we can just increment the
source string index (indicating that the elements we have one less element to match); however, if we stick by the idea
that the half to the right and including the current is unmatched and the left side is the set of matched then we have a
problem because now we have added an element to the matched set that shouldn't be there. Before dismissing incrementing
the index as a solution its important to consider why we need to maintain the set of matched elements. First off, if we
go back to the idea that we have two indexes: one source index to the left of which the source is matched and to right
inclusive of the index the source is matched) and we have one destination index to the left of which the destination is
matched and to the right inclusive of the index the destination is unmatched, we know both the source and destination
,mathched parts are the same, so we dont need to maintain the left half of the source string as the matched set because
its already maintained in the destination string. It was a long detour but essentially that means that incrementing just
our source index (under the premise that the right of the source string index is our unmatched and the left of our
destination is the matched so far) is safe on a delete because it modifies the unmatched set (by decreasing it by one)
and doesnt change the matched set (because it does nothing to the destination index).

The last thing we can try is inserting. Once again to come up with a way to model inserting we need to understand what
an insertion means in the context of this problem. When we insert we are increasing the number of elements that are
matched (by adding a gurantted matched element at out index - i.e the element we are looking at currently in the
destination string ) but not touching the potentially unmatched elements. The fact that we are only changing the matched
element set shows that we only need to mess with the destination index (bc we know all the elements to the left of the
destination index are matched), so we can increment the source index to increase the matched set by the element we are
looking at in the destination (the one we just added), and because we haven't changed the destination index this doesnt
effect the potentially unmatched elements).

We can then repeat this problem of adding elements to our matched and/or decreasing from our unmatched with either noops
, insertions, deletions, or modifications until we have no potentially unmatched and we have a full matched set.

Important to note that is that if we have no potentially unmatched (source index is over the last valid index in the
 string) left but not a full matched set ( destination index is over the last valid index in the string) then we are
forced to do insertions until we get a fully matched set. Also if we have a fully matched set and we have potentially
unmatched elements we knwo that they are all unmatched elements (cus nothing to match anymore) and we need to just
delete all of them.

Step 1: Recursive Contract

Because we are essentially repeating the same process every single time and slowly whettling down our problem until we
hit cases where there we are done or is there is only one solution. This seems like a problem ripe for a recursive sol.
The parameters of every call are the four items we used in the logic described above: source_string, source_index,
destination_string, and destination_index, we didnt talk about it much before but after we also need to increment the
running cost with each choice we make and in order to increment the cost we will need the cost of each operation we can
try. So the job of our recursive function is to given all eight parameters to find the lowest cost (distance) that we
can transform the string in. Here it may seem like a daunting test for the function to have to solve the entire problem
but essentially it needs to make several choices and see which one of them eventually led to the best outcome and return
that, so after it makes a choice and it gets the rest of the cost from another call to itself it adds the cost of its
choice and does this for every choice and then sees which cost is the lowest.

Recursive Contract

Job: Given src_i, src_str, des_i, des_str, current_cost, insertion_cost, modification_cost, deletion cost return best
totalcost

When is the answer obvious:

If there are no potentially unmatched elements:

return the insertion_cost*matches_remaining

If there are no matches remaining:
return insertion_cost*unmatched_elements


General Case:

If the characters are already matched then we can just increment both indices by one and have no cost

if they aernt matched we want to try every fix and adjust our current cost with the price of that fix

Step 2: Pseudo code
"""


def edit_distance(src_str, des_str, insertion_cost, deletion_cost, modification_cost):
    return ed_dp(0, src_str, 0, des_str, 0, insertion_cost, deletion_cost, modification_cost)


def ed_helper(src_i, src_str, des_i, des_str, current_cost, ins_cost, del_cost, mod_cost):
    # Handle base cases
    # no more potentially unmatched elements
    if src_i == len(src_str):
        # we have to do inserts at this point
        return current_cost + ins_cost * (len(des_str) - des_i)

    # no more elements to match
    if des_i == len(des_str):
        # we have to do deletes at this point
        return current_cost + del_cost * (len(src_str) - src_i)

    # we want to check if they are the same and if that is the case just increment the two indices

    if des_str[des_i] == src_str[src_i]:
        return ed_helper(src_i + 1, src_str, des_i + 1, des_str, current_cost, ins_cost, del_cost, mod_cost)

    # We want to make a every possible choice and see what the min is if the indices are different

    # lets try insertion first
    minimum = ed_helper(src_i, src_str, des_i + 1, des_str, ins_cost + current_cost, ins_cost, del_cost,
                        mod_cost)

    # lets try deletion next
    cost_with_deletion = ed_helper(src_i + 1, src_str, des_i, des_str, del_cost + current_cost, ins_cost, del_cost,
                                   mod_cost)

    # lets see which is smaller out of the two of them

    if minimum > cost_with_deletion:
        minimum = cost_with_deletion

    # lets try modification

    cost_with_modification = ed_helper(src_i + 1, src_str, des_i + 1, des_str, mod_cost + current_cost, ins_cost,
                                       del_cost, mod_cost)

    # lets see if its smaller than current min

    if minimum > cost_with_modification:
        minimum = cost_with_modification

    return minimum

"""
lets convert this problem into a dp using the 4 dp steps

1. Figure out properties of table (size, dimension, type)
2. Initialize the table (add the base cases so you can build the rest of the table
3. Figure out the direction of traversal
4. Fill the DP Table


Figure out the properties of the table

Size

We know that well need a table cell for every unique sub problem we are solving so we need to see what parameters are
changing we know that src_i is changing and that des_i is changing and we know we have 0...len(src_str) values for src_i
and we know that we have 0...len(des_str) values for des_str so we will need (len(src_str) + 1)*(len(des_str) + 1) cells
total. 

Dimension we know we need to access the subproblem by src_i and des_i both so it makes sense that we have a two
dimensional array: (len(src_str) + 1)*(len(des_str) + 1)

What we are trying to solve with each subproblem is its min value given its parameters so what we need to store in the 
array is the integer value that is solution it returns

Initialize the table

Here we know that we are trying to fill in the table for the subproblems where the solutions requires no additional info

If there are no potentially unmatched elements:

return the insertion_cost*matches_remaining

no potentially umnatched elements => src_i = len(src_str)

matches remaining => (len(des_str) - des_i)

So we want to fill the last row in our table by movinf down the row and using the col val to get the cell value


If there are no matches remaining:
return insertion_cost*unmatched_elements

no matches remaining => des_i = len(des_str)

unmatched elements = len(src_str) - src_i

So we want to fill the last column of our array by moving down the col and using the row val to get the call value

Figure out the direction of travel 

First lets figure out what the parent problem maps to

We know the parent problem maps to the subproblem in which src_i and des_i are zero and we know that this is our
destination

We for any problem we are looking to solve (given the characters at the indices are not the same) to we know we need the
solution to the subproblems:

if we try an insertion => src_i, des_i + 1 => moving right one column in the table 

if we try a deletion => src_i + 1, des_i => moving down one row

if we try a modificiation => src_i + 1, des_i + 1 => moving diagonally down the table to the right

The first cell that we can be gurantteed has a cell to the right, beneath, and diagnol from it that is filled is the 
bottom right corner (second to last row, second to last col => table[len(src_str) - 1][len(des_str) -1]

The next cells that now have the same conditions satisfied are table[len(src_str) - 1][len(des_str) -2] 
table[len(src_str) - 2][len(des_str) -1] 

and we can keep growing this out until we reach the begining of the second to last row or the top of the second to last
column

we can then repeat this for each row and col in the table 

Fill the DP table

so now that we know in what order the table needs to be filled all that remains to be decided is how to fill a single
cell, so just like our recursion we need to take the minimum of the left, bottom, and diaganolly down cell but we cant
just take the value of the cell we need to increment it without the cost of the operation so the right cell would cost
the value of the right cell plus the cost of an insertion, the bottom cell would cost the the value of the bottom cell
as well as the cost of deletion, and the right diagonal cell would cost the value of the right diaganol plus the cost of
modification

Pseudocode 
"""
def ed_dp(src_i, src_str, des_i, des_str, current_cost, ins_cost, del_cost, mod_cost):
    # Create the DP table
    # table_row = [None] * (len(des_str) + 1)
    # table = [table_row] * (len(src_str) + 1)
    table = [[(x,y) for x in range(len(des_str) + 1)] for y in range(len(src_str) + 1)]


    
    # Initialize the DP table


    # if we have run out potentially unmatched elements then we need to insert the elements left to match

    for des_i in range(0, len(des_str) + 1):
        table[len(src_str)][des_i] = (len(des_str) - des_i) * ins_cost

    # if we have run out of elements to match we need to delete the remaining potentially elements
    for src_i in range(0, len(src_str) + 1):
        table[src_i][len(des_str)] = (len(src_str) - src_i) * del_cost

        
    # start at the second bottom right corner and grow left and up
    src_i = len(src_str) - 1
    des_i = len(des_str) - 1
    while src_i >= 0 and des_i >= 0:
        unfilled_corner_des_i = des_i
        unfilled_corner_src_i = src_i
        
        # Grow the table left
        while des_i >= 0:
            if src_str[src_i] == des_str[des_i]:
                # we dont need to do anything and can assume we have just moved down both arrays at no cost
                table[src_i][des_i] = table[src_i + 1][des_i + 1]
            else:
                table[src_i][des_i] = calc_cell_val(table[src_i][des_i + 1],     ins_cost,
                                                    table[src_i + 1][des_i],     del_cost,
                                                    table[src_i + 1][des_i + 1], mod_cost)
            des_i -= 1

        des_i = unfilled_corner_des_i

        # Grow the table up
        while src_i >= 0:
            if src_str[src_i] == des_str[des_i]:
                # we dont need to do anything and can assume we have just moved down both arrays at no cost
                table[src_i][des_i] = table[src_i + 1][des_i + 1]
            else:
                table[src_i][des_i] = calc_cell_val(table[src_i][des_i + 1],     ins_cost,
                                                    table[src_i + 1][des_i],     del_cost,
                                                    table[src_i + 1][des_i + 1], mod_cost)
            src_i -= 1

        src_i = unfilled_corner_src_i

        src_i -= 1
        des_i -= 1

    # print_table(table)
    # print(traverse_table(table, src_str, des_str, ins_cost, del_cost, mod_cost))
    return table[0][0]


def calc_cell_val(ins_cell, ins_cost, del_cell, del_cost, mod_cell, mod_cost):
    minimum = ins_cell + ins_cost
    delete = del_cell + del_cost
    modify = mod_cell + mod_cost

    if minimum > delete:
        minimum = delete

    if minimum > modify:
        minimum = modify

    return minimum

def get_next_cell(table, src_i, des_i, ins_cost, del_cost, mod_cost, src_str, des_str):

    if src_str[src_i] == des_str[des_i]:
        return src_i + 1, des_i + 1, "noop"

    next_cell_val =  calc_cell_val(table[src_i][des_i + 1],     ins_cost,
                                   table[src_i + 1][des_i],     del_cost,
                                   table[src_i + 1][des_i + 1], mod_cost)

    if next_cell_val == table[src_i][des_i + 1] + ins_cost:
        return src_i, des_i + 1, "insertion"

    if next_cell_val == table[src_i + 1][des_i] + del_cost:
        return src_i + 1, des_i, "deletion"

    if next_cell_val == table[src_i + 1][des_i + 1] + mod_cost:
        return src_i + 1, des_i + 1, "modification"




def print_table(table):
    print("_____________________________________________")
    for row in table:
        print(row)
    print("_____________________________________________")


def traverse_table(table, src_str, des_str, ins_cost, del_cost, mod_cost):
    src_i = 0
    des_i = 0
    ops = []
    # print(src_str)
    # print(des_str)
    print(get_modified_str(src_str, src_i, des_str, des_i))
    while src_i < len(src_str) and des_i < len(des_str):
        # print(src_str[src_i] + ", " + des_str[des_i])

        src_i, des_i, op = get_next_cell(table, src_i, des_i, ins_cost, del_cost, mod_cost, src_str, des_str)
        print(get_modified_str(src_str, src_i, des_str, des_i))
        # print(op)
        ops.append(op)
    if src_i == len(src_str):
        num_dels = table[src_i][des_i]/ins_cost
        for i in range(num_dels):
            des_i += 1
            print(get_modified_str(src_str, src_i, des_str, des_i))
            ops.append("insertion")

    if des_i == len(des_str):
        num_ins = table[src_i][des_i]/del_cost
        for i in range(num_ins):
            src_i += 1
            print(get_modified_str(src_str, src_i, des_str, des_i))
            ops.append("deletion")

    return ops

def get_modified_str(src_str, src_i, des_str, des_i):
    char_arr = []
    for i in range(0, des_i):
        char_arr.append(des_str[i])
    for i in range(src_i, len(src_str)):
        char_arr.append(src_str[i])
    return "".join(char_arr)

print(edit_distance("cat", "bud", 1, 2, 0))
