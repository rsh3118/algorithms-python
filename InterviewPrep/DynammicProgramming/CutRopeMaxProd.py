"""
Step 0: get to know the problem

if we have rope of length 4

we can start cutting it as follows

1, 1, 1, 1 => 1
1, 1, 2 => 2
1, 2, 1 => 2
1, 3 => 3
2, 1, 1 => 2
2, 2 => 4
3, 1 => 3
4 => 4

So we essentially did here is we tried cutting a segment of rope and then we saw what the best we could do with that was

Then we choose which was the best of those

However, there were alot of repitions because essentially everytime we are saying whats the best we can do with some m
lenght of rope, where m is less than m

But whats most important here is that we tried every possible cut we cud make and then teied to see what what was the
best we could with the remaining part (which we did by repeating that process)

so we tried all the cuts from 1, to n and saw the best multiplicative outcome would be

Step 1: Recursive Contract

Contract: Tell us the best we can do with rope of length l

General Cases: Pick all lenghts m from 1 to l and then multiply m * f(l-m) to get prod and take the m which delivers the
highest prod

Base Cases: with length of 1 the best you can do is 1

Step 2: Determine if its a DP problem

here we are determined whats the best we could do with some m and it doesnt matter how we get to m because we used the
value of that expression doesn't depend on what came before it

There are a lot of ways to get m and all the ms we want will be contiguous because we at some point will try to make all
one size cuts so we will have calculated what the best we can do for each m from 1 to n so we wont do a lot of extra
function calls

Step 3: Pseudo code

"""


def max_product_from_cut_pieces(n):
    # Figure out the size of the table

    # The function will be called for exactly every value between 1 and n (inclusive) [size = n + 1]
    # There is only one changing input that matters between the calls (n) so it can be a one dimensional solution
    # We are returning the max value given the size of the rope so the type of the table is int
    table = [None] * (n + 1)

    # Initialize the table
    # Using the base cases we know if we have rope of size 1 the answer is 1
    # here we can set table zero to 1 which means in what the length would be in the case where the cut we used is the
    # entire rope and so we should be able to multiple the lneght by 1 (bc this mul. id. it should be fine)

    table[0] = 0
    table[1] = 1

    # Direction of traversal: We know that for any element to figure out we can do we are gonna make all possible cuts
    # so essentially we are gonna try to reduce it to every lenght between 1 and less than n
    # here we know we already have 1 so we can start with 2 because we have all the elements between 1 and 2
    # then we can do 3 because we will have all elements less than it ( and we can keep repeating this process)

    for m in range(2, n + 1):

        # Fill the DP: here we want to take the max of the cut times all the cuts that come before it so we have to
        # try every cut and get the product and take the max to get the value of the cell, however we dont want to
        max_prod = 0
        for cut in range(1, m):
            prod = cut * table[m - cut]
            if prod > max_prod:
                max_prod = prod
            # no cut case
            prod = cut * (m - cut)
            if prod > max_prod:
                max_prod = prod

        table[m] = max_prod

    print(table)

    return table[n]


print(max_product_from_cut_pieces(105))
