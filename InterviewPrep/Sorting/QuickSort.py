def choose_pivot(array, start, end):
    """
    returns the index of the chosen pivot
    :param array:
    :param start:
    :param end:
    :return:
    """
    return start


def partition(array, start, end, pivot):
    """
    returns the final position of the pivot after partitioning the array
    :param array:
    :param start:
    :param end:
    :param pivot:
    :return:
    """
    pivot_position = 0
    pivot_element = array[pivot]
    for element in array:
        if element < pivot_element:
            pivot_position += 1
    new_array = [None] * array.length
    index = 0
    less_index = 0
    greater_index = pivot_position + 1
    new_array[pivot_position] = pivot_element
    for element in array:
        if index == pivot_position:
            pass
        else:
            if element <= pivot_element:
                new_array[less_index] = element
                less_index += 1
            else:
                new_array[greater_index] = element
                greater_index += 1
        index += 1




    return start


def quicksort(array, start, end):
    if start >= end:
        return
    else:
        initial_pivot = choose_pivot(array, start, end)
        final_pivot = partition(array, start, end, initial_pivot)
        quicksort(array, start, final_pivot - 1)
        quicksort(array, final_pivot + 1, end)

def quick_select(array, rank, start, end):
    initial_pivot = choose_pivot(array, start, end)
    pivot = partition(array, start, end, initial_pivot)
    if pivot == (rank -1):
        return array[pivot]
    if pivot < (rank -1):
        start = pivot + 1
    if pivot > (rank -1):
        end = pivot - 1
    quick_select(array, rank, start, end)

def is_sorted_subset(sorted_array_1, sorted_array_2):
    subset_candidate = []
    superset_candidate = []
    if sorted_array_1.length < sorted_array_2.length:
        subset_candidate = sorted_array_1
        superset_candidate = sorted_array_2
    if sorted_array_1.length == sorted_array_2.length:
        subset_candidate = sorted_array_1
        superset_candidate = sorted_array_2
    if sorted_array_1.length > sorted_array_2.length:
        subset_candidate = sorted_array_2
        superset_candidate = sorted_array_1
    subset_index = 0
    superset_index = 0
    while superset_index < len(superset_candidate) and subset_index < len(subset_candidate):
        if subset_candidate[subset_index] == superset_candidate[superset_index]:
            subset_index += 1
        superset_index += 1
    if subset_index != len(subset_candidate):
        return False
    return True


partition()