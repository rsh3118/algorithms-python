def merge_sort(array, start, end):
    if start == end:
        return
    mid = start + (end - start) / 2
    merge_sort(array, start, mid)
    merge_sort(array, mid + 1, end)
    merge(array, start, mid, end)


def merge(array, start, mid, end):
    new_array = []
    # first half array temp index
    fh_index = start
    # second half array temp index
    sh_index = mid + 1
    while fh_index <= mid or sh_index <= end:
        appendFromFirst = True
        if fh_index > mid:
            appendFromFirst = False
        else:
            if array[fh_index] <= array[sh_index]:
                pass
            else:
                appendFromFirst = False

        if appendFromFirst:
            new_array.append(array[fh_index])
            fh_index += 1
        else:
            new_array.append(array[sh_index])
            sh_index += 1
    index = 0
    for element in new_array:
        array[index] = element
        index += 1






arr = [1, 3, 5, 2, 4, 6]
merge(arr, 0, 2, 5)
print(arr)
