class Node:
    left = None
    right = None

    def __init__(self, val):
        self.val = val


def topK(arr, k):
    if k == 0:
        return
    root = None
    num_nodes = 0
    for num in arr:
        #print(num)
        if num_nodes < k and not bst_has_num(root, num):
            root = add_to_bst(root, num)
            num_nodes += 1
        else:
            parent, child = get_min(root)
            min_val = child.val
            if min_val < num and not bst_has_num(root, num):
                # print("adding")
                root = delete_min_node(root, parent, child)
                root = add_to_bst(root, num)
        #print(get_array(root))
    return get_array(root)

def bst_has_num(node, num):
    if node is None:
        return False
    num_on_left = False
    num_on_right = False
    if node.val == num:
        return True
    if node.left is not None:
        num_on_left = bst_has_num(node.left, num)
    if node.right is not None:
        num_on_right = bst_has_num(node.right, num)
    return num_on_left or num_on_right


def get_array(node):
    left_array = []
    right_array = []
    if node.left is not None:
        left_array = get_array(node.left)
    if node.right is not None:
        right_array = get_array(node.right)
    left_array.append(node.val)
    left_array.extend(right_array)
    return left_array


def add_to_bst(node, num):
    if node is None:
        return Node(num)
    # print("Node:{}, left:{}, right:{} ".format(node.val, node.left, node.right))
    if node.val > num:
        if node.left is None:
            node.left = Node(num)
        else:
            add_to_bst(node.left, num)
    if node.val == num:
        return node
    if node.val < num:
        if node.right is None:
            node.right = Node(num)
        else:
            add_to_bst(node.right, num)
    return node


def delete_min_node(root, parent_node, child_node):
    # print("parent:{}, child:{}".format(parent_node.val, child_node.val))
    if parent_node == child_node:
        return child_node.right
    if child_node.left is not None:
        raise Exception("Not a mimimum")
    parent_node.left = child_node.right
    return root


def get_min(root):
    parent = None
    min = None
    node = root
    if root.left is None:
        #print("root is min")
        return root, root
    while node.left is not None:
        parent = node
        node = node.left
    return parent, node






def topK_linear(arr, k):
    # dont forget to code for the edge cases where k is 1,2
    if k == 0:
        return
    min_heap = []
    for num in arr:
        if len(min_heap) < k:
            add_to_heap(min_heap, num)
        else:
            min_val = min_heap[0]
            if min_val < num:
                delete_min(min_heap)
                add_to_heap(min_heap, num)
    return min_heap


def add_to_heap(heap, num):
    heap.append(num)
    new_index = len(heap) - 1
    parent_index = get_parent(new_index)
    while heap[parent_index] > heap[new_index]:
        swap(heap, parent_index, new_index)
        new_index = parent_index
        parent_index = get_parent(new_index)


def delete_min(heap):
    temp_min = heap[len(heap) - 1]
    heap.pop()
    if len(heap) == 0:
        return
    heap[0] = temp_min
    i = 0
    nll = node_less_left(heap, i)
    nlr = node_less_right(heap, i)
    while not (nll and nlr):
        if not nll and nlr:
            swap(heap, i, get_left(i))
            i = get_left(i)
        if nll and not nlr:
            swap(heap, i, get_right(i))
            i = get_right(i)
        if not nll and not nlr:
            # we know for both of them must exist for both to be false
            if heap[get_left(i)] <= heap[get_right(i)]:
                swap(heap, i, get_left(i))
                i = get_left(i)
            else:
                swap(heap, i, get_right(i))
                i = get_right(i)
        nll = node_less_left(heap, i)
        nlr = node_less_right(heap, i)


def node_less_left(heap, parent_index):
    left_index = get_left(parent_index)
    if left_index < len(heap):
        return heap[parent_index] <= heap[left_index]
    return True


def node_less_right(heap, parent_index):
    right_index = get_right(parent_index)
    if right_index < len(heap):
        return heap[parent_index] <= heap[right_index]
    return True


def get_parent(child_index):
    if child_index == 0:
        return 0
    if is_even(child_index):
        return (child_index - 2)/2
    return (child_index - 1)/2


def get_left(parent_index):
    return parent_index*2 + 1


def get_right(parent_index):
    return parent_index*2 + 2


def is_even(num):
    if num % 2 == 1:
        return False
    return True


def swap(arr, a, b):
    temp = arr[a]
    arr[a] = arr[b]
    arr[b] = temp

array = [4, 8, 9, 6, 6, 2, 10, 2, 8, 1, 2, 9]

print(topK(array, 7))
