def solve(arr):
    last_even = -1
    first_odd = len(arr)
    i = 0
    while(last_even + 1 < first_odd):
        if is_even(arr[i]):
            last_even += 1
            i += 1
        else:
            swap(arr, i, first_odd - 1)
            first_odd -= 1


def is_even(num):
    if num % 2 == 1:
        return False
    return True


def swap(arr, a, b):
    temp = arr[a]
    arr[a] = arr[b]
    arr[b] = temp

array = []
solve(array)
print(array)