#
# Complete the merger_first_into_second function below.
#


def merger_first_into_second(arr1, arr2):
    #
    # Write your code here.
    #
    insertion_index = len(arr2) - 1
    current_arr1_index = len(arr1) - 1
    current_arr2_index = len(arr1) - 1
    while current_arr1_index >= 0 and current_arr2_index >= 0:
        if arr1[current_arr1_index] >= arr2[current_arr2_index]:
            arr2[insertion_index] = arr1[current_arr1_index]
            current_arr1_index -= 1
            insertion_index -= 1
        else:
            arr2[insertion_index] = arr2[current_arr2_index]
            current_arr2_index -= 1
            insertion_index -= 1
    while current_arr1_index >= 0:
        arr2[insertion_index] = arr1[current_arr1_index]
        current_arr1_index -= 1
        insertion_index -= 1
    return arr2


arr1 = [1, 2, 3]
arr2 = [3, 7, 9, 0, 0, 0]
print(merger_first_into_second(arr1, arr2))
