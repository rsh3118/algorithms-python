#
# Complete the dutch_flag_sort function below.
#


def swap(array, a_index, b_index):
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp


def dutch_flag_sort(balls):
    """
    Here the general idea would be to use something similar to the partition method where the lesser and greater
    subarrays grow in except here we can use instead of < = and > as the subarray groupings we can use 'R' 'G' 'B'
    :param balls:
    :return:
    """
    balls = list(balls)
    r_end_index = -1
    b_start_index = len(balls)
    index = 0
    while True:
        if index == b_start_index:
            return ''.join(balls)
        if balls[index] == 'R':
            swap(balls, r_end_index + 1, index)
            r_end_index += 1
            index += 1
        elif balls[index] == 'B':
            swap(balls, index, b_start_index - 1)
            b_start_index -= 1
        else:
            index += 1

balls = "GBGGRBRG"
print(dutch_flag_sort(balls))
