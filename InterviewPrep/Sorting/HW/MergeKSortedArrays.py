import math


class BTNode:
    left = None
    right = None

    def __init__(self, data):
        self.data = data

    def __add__(self, other):
        if self.left is None:
            self.left = other
        else:
            if self.right is None:
                self.right = other
            else:
                raise Exception("Node already has left and right child")

    def __str__(self):
        return str(self.data)


def default_find_spot(node, element):
    """
    Creates a left complete binary tree that has no inherent ordering
    :param node: root
    :param element: value of element to be added
    :return: void
    """
    queue = [node]
    while True:
        if node.left is None:
            node.left = BTNode(element)
            return
        else:
            queue.append(node.left)
        if node.right is None:
            node.right = BTNode(element)
            return
        else:
            queue.append(node.right)
        node = queue.pop(0)


class BT:
    root = None

    def __init__(self, array, insertion_function=default_find_spot):
        self.find_spot = insertion_function
        for element in array:
            self + element;

    def __add__(self, other):
        if self.root is None:
            self.root = BTNode(other)
            return
        current_node = self.root
        self.find_spot(current_node, other)

    def __str__(self):
        return self.create_formatted_tree_string()

    def create_formatted_tree_string(self):
        if self.root == None:
            return ""
        depth = 0
        index = 0
        queue = [(depth, self.root, index)]
        node_level_map = dict()
        node_level_map[depth] = [self.root.data]
        max_buffer_length = 4
        tree_depth = 0
        while len(queue) > 0:
            current_tuple = queue.pop(0)
            depth = current_tuple[0] + 1
            parent_node = current_tuple[1]
            left_child = parent_node.left
            right_child = parent_node.right
            parent_index = current_tuple[2]
            if depth not in node_level_map and (left_child is not None or right_child is not None):
                tree_depth = max(tree_depth, depth)
                node_level_map[depth] = []
            if left_child is not None:
                max_buffer_length = max(max_buffer_length, len(str(left_child.data)))
                is_right_node = False
                node_level_map = self.update_node_level_map(left_child, parent_index, depth, node_level_map,
                                                            is_right_node)
                queue = self.update_queue(left_child, parent_index, depth, queue, is_right_node)
            if right_child is not None:
                max_buffer_length = max(max_buffer_length, len(str(right_child.data)))
                is_right_node = True
                node_level_map = self.update_node_level_map(right_child, parent_index, depth, node_level_map,
                                                            is_right_node)
                queue = self.update_queue(right_child, parent_index, depth, queue, is_right_node)
        for i in range(len(node_level_map)):
            ideal_node_array_len = 2 ** i
            while len(node_level_map[i]) < ideal_node_array_len:
                node_level_map[i].append(None)
        tree_str = ""
        temp_depth = 0;
        while temp_depth <= tree_depth:
            tree_level_str = self.format_level(tree_depth, node_level_map[temp_depth], max_buffer_length)
            tree_str += tree_level_str
            tree_str += "\n"
            temp_depth += 1
        return tree_str

    def update_queue(self, current_node, parent_index, current_depth, queue, is_right_node):
        node_index = (2 * parent_index + 1)
        if is_right_node:
            node_index += 1
        queue.append((current_depth, current_node, node_index))
        return queue

    def update_node_level_map(self, current_node, parent_index, current_depth, node_level_map, isRightNode):
        node_index = (2 * parent_index + 1)
        if isRightNode:
            node_index += 1
        array_index = node_index - (2 ** current_depth - 1)
        node_level_map[current_depth] = self.insert_with_empty(node_level_map[current_depth], array_index,
                                                               current_node.data)
        return node_level_map

    def insert_with_empty(self, array, index, element):
        endIndex = len(array) - 1
        while endIndex < index:
            endIndex = endIndex + 1
            array.insert(endIndex, None)
        array[index] = element
        return array

    def format_level(self, tree_depth, node_level_array, buffer_length):
        level_str = ""
        num_of_lowest_level_nodes = 2 ** tree_depth
        space_for_lowest_level_nodes = num_of_lowest_level_nodes * 2 - 1
        depth_of_node_level_array = math.log(len(node_level_array)) / math.log(2)
        depth_of_each_subtree_on_level = tree_depth - depth_of_node_level_array
        num_leaves_of_each_subtree_on_level = 2 ** depth_of_each_subtree_on_level
        space_for_leaves_of_each_subtree_on_level = num_leaves_of_each_subtree_on_level * 2 - 1
        num_of_subtrees_on_level = len(node_level_array)
        """
        we always know that we need half of the space for the first subtrees elements to be to the left of the first
        subtrees root node minus the space separating it from the other half (as that space will be provided by the
        subtrees root node) so to accomplish this we can just take the lower bound of the half the space the leaves of 
        that subtree will take
        """
        level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level / 2)
        level_str += self.format_node(node_level_array.pop(0), buffer_length)
        """
        between adjacent root nodes we always need space for the right half of the left root's subtree and we need 
        space for the left half of the right root nodes subtree and we want to make sure to we do thus until we have 
        completed all adjacent nodes
        """
        while len(node_level_array) > 0:
            level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level)
            root_node_value = self.format_node(node_level_array.pop(0), buffer_length)
            level_str += root_node_value
        level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level / 2)
        if len(level_str) != space_for_lowest_level_nodes * buffer_length:
            raise Exception(
                "The formatted binary tree level string is not as long as the lowest level string\n{} is of size {} but it should have been of size {}".format(
                    level_str, len(level_str), space_for_lowest_level_nodes * buffer_length))
        return level_str

    def format_node(self, value, buffer_length):
        if value is not None:
            node_data_str = str(value)
        else:
            node_data_str = "None"
        if len(node_data_str) > buffer_length:
            raise ValueError(
                "Insufficient Buffer Length\nNeed buffer lenght of size {} for {} but found buffer of size {}".format(
                    len(node_data_str), value, buffer_length))
        num_beg_empty_spaces = int((buffer_length - len(node_data_str)) / 2)
        node_buffer_str = " " * int((buffer_length - len(node_data_str)) / 2)
        node_buffer_str += node_data_str
        num_end_empty_spaces = buffer_length - len(node_buffer_str)
        node_buffer_str += " " * num_end_empty_spaces
        return node_buffer_str


class BST(BT):
    def __init__(self, array):
        BT.__init__(self, array, bst_find_spot)


def bst_find_spot(node, element):
    if (node.data > element) and (node.left is None):
        node.left = BTNode(element)
    if (node.data < element) and (node.right is None):
        node.right = BTNode(element)
    if (node.data > element) and (node.left is not None):
        bst_find_spot(node.left, element)
    if (node.data < element) and (node.right is not None):
        bst_find_spot(node.right, element)



def is_element_min_heap_element(array, parent_index, child_index):
    return array[parent_index].val <= array[child_index].val

def is_element_max_heap_element(array, parent_index, child_index):
    return array[parent_index].val >= array[child_index].val

def get_min_swap_index(array, element_index):
    left_child_index = get_left_child_index(element_index)
    right_child_index = get_right_child_index(element_index)
    if not is_index_valid(array, left_child_index):
        raise Exception("There are no children to swap with")
    if not is_index_valid(array, right_child_index):
        return left_child_index
    swap_index = left_child_index if array[left_child_index].val <= array[
        right_child_index].val else right_child_index
    return swap_index

def get_max_swap_index(array, element_index):
    left_child_index = get_left_child_index(element_index)
    right_child_index = get_right_child_index(element_index)
    if not is_index_valid(array, left_child_index):
        raise Exception("There are no children to swap with")
    if not is_index_valid(array, right_child_index):
        return left_child_index
    swap_index = left_child_index if array[left_child_index] >= array[
        right_child_index] else right_child_index
    return swap_index

class Node:
    val= None
    parent_list_index = None
    def __init__(self, val, parent_list_index):
        self.val = val
        self.parent_list_index = parent_list_index

    def __str__(self):
        return str(self.val)

class MinHeap:
    array = []

    def __init__(self, get_swap_index=get_min_swap_index, is_element_heap_element=is_element_min_heap_element):
        self.get_swap_index = get_swap_index
        self.is_element_heap_element= is_element_heap_element

    def insert(self, element):
        """

        :param element:
        :return: void
        """
        self.array.append(element)
        self.heapify_up()

    def heapify_up(self):
        """

        :return: void
        """
        element_index = len(self.array) - 1
        if element_index == 0:
            return
        parent_index = get_parent_index(element_index)
        while not self.is_element_heap_element(self.array, parent_index, element_index):
            swap(self.array, parent_index, element_index)
            element_index = parent_index
            if element_index == 0:
                return
            parent_index = get_parent_index(parent_index)

    def heapify_down(self):
        """

        :return:
        """
        print(self)
        element_index = 0
        if node_has_children(self.array, element_index):
            swap_index = self.get_swap_index(self.array, element_index)
        else:
            return
        heap_appropriate = self.is_element_heap_element(self.array, element_index, swap_index)
        while not heap_appropriate:
            swap(self.array, element_index, swap_index)
            print(self)
            element_index = swap_index
            if node_has_children(self.array, element_index):
                swap_index = self.get_swap_index(self.array, element_index)
                heap_appropriate = self.is_element_heap_element(self.array, element_index, swap_index)
            else:
                return

    def delete_top(self):
        """

        :return: element
        """
        top_element = self.array[0]
        last_element = self.array.pop(-1)
        if(len(self.array) > 0):
            self.array[0] = last_element
        self.heapify_down()
        return top_element

    def __str__(self):
        val_array = []
        for element in self.array:
            val_array.append(element.val)
        return str(BT(val_array))





def swap(array, a_index, b_index):
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp


def get_parent_index(child_index):
    return int((child_index - 1) / 2)


def get_left_child_index(parent_index):
    return 2 * parent_index + 1


def get_right_child_index(parent_index):
    return 2 * parent_index + 2


def is_index_valid(array, index):
    return index < len(array)


def node_has_children(array, index):
    return (2 * index + 1) < len(array)

#
# Complete the mergeArrays function below.
#
def mergeArrays(arr):
    #
    # Write your code here.
    #
    heap = MinHeap()
    list_of_list_indices = [0] * len(arr)
    for parent_list_index in range(len(arr)):
        index_of_current_list = list_of_list_indices[parent_list_index]
        current_list = arr[parent_list_index]
        current_val_of_current_list = current_list[index_of_current_list]
        heap.insert(Node(current_val_of_current_list, parent_list_index))
        index_of_current_list += 1
        list_of_list_indices[parent_list_index] = index_of_current_list
    merged_array = []
    while len(heap.array) > 0:
        node = heap.delete_top()
        print(heap)
        merged_array.append(node.val)
        index_of_current_list = list_of_list_indices[node.parent_list_index]
        current_list = arr[node.parent_list_index]
        if index_of_current_list < len(current_list):
            current_val_of_current_list = current_list[index_of_current_list]
            node = Node(current_val_of_current_list, node.parent_list_index)
            print(node)
            heap.insert(Node(current_val_of_current_list, node.parent_list_index))
            print(heap)
            index_of_current_list += 1
            list_of_list_indices[node.parent_list_index] = index_of_current_list
    return merged_array
arr = [None] * 10
arr[0] = [ 4, 4, 7, 11, 13, 20, 26, 34]
arr[1] = [ 0, 8, 10, 19, 23, 27, 34, 41]
arr[2] = [5, 7, 7, 7,12, 19, 25, 26]
arr[3] = [9, 12, 19, 27, 33, 35, 39, 46]
arr[4] = [0, 3, 10, 18, 18, 22, 24, 33]
arr[5] = [9, 12, 20, 21, 30, 35, 35, 42]
arr[6] = [7, 8, 12, 12, 21, 24, 33, 42]
arr[7] = [7, 8, 11, 18, 18, 21, 23, 29]
arr[8] = [8, 14, 15, 23, 30, 30, 35]
arr[9] = [4, 5, 11, 12, 16, 17, 18, 20]


print(mergeArrays(arr))






