#
# Complete the solve function below.
#
def solve(nuts, bolts):
    """
    General Idea

    Partition nuts using bolt pivot and partition bolts using
    nut pivot until both arrays are sorted
    :param nuts:
    :param bolts:
    :return:
    """
    start = 0
    end = len(bolts) - 1
    partition_nuts(nuts, start, len(nuts) - 1, bolts)

def get_middle_index(smallest_index, largest_index):
    return smallest_index + (largest_index - smallest_index)/2



def swap(array, a_index, b_index):
    """
    swaps the elements at the two indices
    :param array:
    :param a_index:
    :param b_index:
    :return:
    """
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp


def partition_nuts(nuts, start, end, bolts):
    """

    :param bolt:
    :param nuts:
    :return:
    """
    if start >= end:
        return
    bolt_partition_index = get_middle_index(start, end)
    nut_partition_index = partition(bolts[bolt_partition_index], nuts, start, end)
    """
    at this point we know that nuts are partitioned and that the value associated with the 
    corresponding nut value associated with the bolt partition index is in the right place
    so we also know that if we choose the nut value (which we know to be at nut partition 
    index) it is the same value as the initial bolt value we got so if we partition the bolts
    based on that value it will end up at the same index as its corresponding nut value because
    both lists are of the same size and each nut has a corresponding bolt
    """
    partition_bolts(nuts[nut_partition_index], bolts, start, end, nuts)


def partition_bolts(nut, bolts, start, end, nuts):
    """

    :param nut:
    :param bolts: list
    :param start:
    :param end:
    :param nuts:
    :return:
    """
    bolt_pivot_index = partition(nut, bolts, start, end)
    """
    at this point we know that nut and bolt lists have both been partitioned on the same element
    so we know their corresponding pivots are at the same indexes and we know everything to the left of
    said pivots are less than everything to the right of said pivots so now we can reduce the problems
    into two parts
    """

    partition_nuts(nuts, start, bolt_pivot_index - 1, bolts)
    partition_nuts(nuts, bolt_pivot_index + 1, end, bolts)


def partition(pivot, array, start, end):
    """

    :param pivot:
    :param array:
    :return: pivot_position
    """
    lesser_end_index = -1
    greater_begin_index = end + 1
    while True:
        element = array[lesser_end_index + 1]
        if element < pivot:
            lesser_end_index += 1
        if element == pivot:
            if (greater_begin_index - 1) == (lesser_end_index + 1):
                return lesser_end_index + 1
            swap(array, lesser_end_index + 1, lesser_end_index + 2)
        if element > pivot:
            swap(array, lesser_end_index + 1, greater_begin_index - 1)
            greater_begin_index -= 1


# array = [1, 4, 5, 6, 21, 3, 7, 9, 8, 10, 11, 12, 14, 2]
# partition_index = get_middle_index(0, len(array) - 1)
# partition_val = array[partition_index]
# print(partition(partition_val, array, 3, len(array) - 3))
# print(array)

nuts = [5,3,2,4,6,7,1]
bolts = [6,2,3,1,4,5,7]
solve(nuts, bolts)
print(nuts)
print(bolts)
