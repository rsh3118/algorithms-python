from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode
from InterviewPrep.Trees.DataStructures.BinaryTree import BT

def findLCA(root, n1, n2):
    # get all the way down to left_root
    (n1f, n1_parent_stack) = find_n(root, n1, [])
    (n2f, n2_parent_stack) = find_n(root, n2, [])
    print(n1_parent_stack)
    print(n2_parent_stack)
    lca = None
    min_length = min(len(n1_parent_stack), len(n2_parent_stack))
    i = 0
    while (n1_parent_stack[i] == n2_parent_stack[i]):
        lca = n1_parent_stack[i]
        i = i + 1
        if i == min_length:
            break
    return lca


def find_n(node, n, parent_stack):
    parent_stack.append(node.data)
    n_left = None
    n_left_parent_stack = []
    n_right = None
    n_right_parent_stack = []
    if node.data == n:
        return (node, parent_stack)
    else:
        if node.left is not None:
            (n_left, n_left_parent_stack) = find_n(node.left, n, parent_stack)
            n_left_parent_stack.append(node.left.data)
            del parent_stack[-1]
        if node.right is not None:
            (n_right, n_right_parent_stack) = find_n(node.right, n, parent_stack)
            n_right_parent_stack.append(node.right.data)
            del parent_stack[-1]
        if n_left is not None and n_right is not None:
            raise Exception("Found n twice")
        if n_left is not None:
            return (n_left, n_left_parent_stack)
        if n_right is not None:
            return (n_right, n_right_parent_stack)
        return (None, [])


bintree = BT([4, 3, 5, 6, 1, 2, 9])
print(bintree)
print(findLCA(bintree.root, 1,6))
