from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode


def postorderTraversal(root):
    stack = [(root, False)]
    postorder = []
    stack_index = 0
    while stack:
        if not stack[stack_index][1]:
            stack[stack_index] = (stack[stack_index][0], True)
            if stack[stack_index][0].right is not None:
                stack.append((stack[stack_index][0].right, False))
            if stack[stack_index][0].left is not None:
                stack.append((stack[stack_index][0].left, False))
        else:
            postorder.append(str(stack[stack_index][0].data))
            del stack[-1]

        stack_index = len(stack) - 1
    # print(postorder)
    return " ".join(postorder)


DN = BTNode("Durnehvir")
OV = BTNode("Odhaving")
SP = BTNode("Saphira")
PX = BTNode("Parthunax")
SK = BTNode("Shruikan")
TH = BTNode("Thorn")
GR = BTNode("Glaedr")

SK.right = TH
SP.right = GR
OV.left = PX
OV.right = SK
DN.left = OV
DN.right = SP

print(postorderTraversal(DN))
