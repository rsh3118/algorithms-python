from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode
from InterviewPrep.Trees.DataStructures.BinaryTree import BT


def reverse_tree(bt):
    reverse_tree_helper(bt.root)
    return bt

def reverse_tree_helper(old_previous_node):
    # new_previous_node = BTNode(old_previous_node.data)
    left = old_previous_node.left
    right = old_previous_node.right
    if old_previous_node.has_right_child():
        old_previous_node.left = reverse_tree_helper(right)
    if old_previous_node.has_left_child():
        old_previous_node.right = reverse_tree_helper(left)
    return old_previous_node


bintree = BT([4, 3, 5, 6, 1, 2, 9, 10, None, None, 3])
print(bintree)
reverse_tree(bintree)
print(bintree)