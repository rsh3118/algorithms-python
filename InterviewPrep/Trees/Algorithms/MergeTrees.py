from InterviewPrep.Trees.DataStructures.BinaryTree import BT
from InterviewPrep.Trees.DataStructures.BinarySearchTree import BST
from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode


def mergeTrees(node1, node2):
    sorted_array_1 = get_sorted_array(node1)
    sorted_array_2 = get_sorted_array(node2)
    merged_array = merge(sorted_array_1, sorted_array_2)
    return constructBST(merged_array)


def get_sorted_array(node):
    left_array = []
    right_array = []
    if node.left is not None:
        left_array = get_sorted_array(node.left)
    if node.right is not None:
        right_array = get_sorted_array(node.right)
    node.left = None
    node.right = None
    left_array.append(node)
    left_array.extend(right_array)
    return left_array

def merge(a1, a2):
    i1 = 0
    i2 = 0
    m = []
    while i1 < len(a1) and i2 < len(a2):
        if a1[i1].data < a2[i2].data:
            m.append(a1[i1])
            i1 += 1
        elif a1[i1].data == a2[i2].data:
            raise Exception("Equal Elements")
        else:
            m.append(a2[i2])
            i2 += 1
    unfinished_array = []
    ui = 0
    if i1 < len(a1) and i2 < len(a2):
        raise Exception("Leftover elements in both arrays")
    if i1 < len(a1):
        unfinished_array = a1
        ui = i1
    if i2 < len(a2):
        unfinished_array = a2
        ui = i2
    while ui < len(unfinished_array):
        m.append(unfinished_array[ui])
        ui += 1
    return m


def constructBST(sorted_array):
    if len(sorted_array) == 0:
        return None
    if len(sorted_array) == 1:
        return sorted_array[0]
    middle_index = len(sorted_array)/2
    node = sorted_array[middle_index]
    node.left = constructBST(sorted_array[:middle_index])
    node.right = constructBST(sorted_array[(middle_index + 1):])
    return node

bt1 = BST([2,8,4,6,10])
bt2 = BST([1,7,3,5,9])
bt1.root = mergeTrees(bt1.root, bt2.root)
print(bt1)

