from InterviewPrep.Trees.DataStructures.BinaryTree import BT
from InterviewPrep.Trees.DataStructures.BinarySearchTree import BST
from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode

def BSTtoLL(node):
    print_BST_LL(BST2LL(node))


def BST2LL(node):
    left_node = None
    right_node = None
    if node.left is not None:
        left_node = to_rightmost(BST2LL(node.left))
        left_node.right = node
    if node.right is not None:
        right_node = to_leftmost(BST2LL(node.right))
        right_node.left = node
    node.left = left_node
    node.right = right_node
    return node


def print_BST_LL(node):
    linked_list = []
    head = to_leftmost(node)
    current = head
    while current is not None:
        linked_list.append(str(current.data))
        current = current.right
    print(" ".join(linked_list))


def to_rightmost(node):
    current = node
    while current.right is not None:
        current = current.right
    return current


def to_leftmost(node):
    current = node
    while current.left is not None:
        current = current.left
    return current

bt = BST([4, 3, 5, 6, 1, 2, 9])
print(bt)
BSTtoLL(bt.root)
