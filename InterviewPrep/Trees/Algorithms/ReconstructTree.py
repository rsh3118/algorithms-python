from InterviewPrep.Trees.DataStructures.BinaryTree import BT
from InterviewPrep.Trees.DataStructures.BinarySearchTree import BST
from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode

def reconstruct_BST_with_preorder(po_str):
    po_str = po_str.replace(" ", "")
    bt_temp = BT([1])
    bt_temp.root = reconstruct_BST_with_preorder_helper(po_str, 0, len(po_str) - 1)
    return bt_temp


def reconstruct_BST_with_preorder_helper(po_str, po_start_index, po_end_index):
    if po_end_index + 1 == po_start_index:
        return None
    root = BTNode(int(po_str[po_start_index]))
    if po_start_index == po_end_index:
        return root
    i = po_start_index + 1
    while i < len(po_str):
        if int(po_str[i]) > root.data:
            break
        i += 1
    left_tree = reconstruct_BST_with_preorder_helper(po_str, po_start_index + 1, i - 1)
    right_tree = reconstruct_BST_with_preorder_helper(po_str, i, po_end_index)
    root.left = left_tree
    root.right = right_tree
    return root

def reconstruct_BST_with_postorder(po_str):
    po_str = po_str.replace(" ", "")
    bt_temp = BT([1])
    bt_temp.root = reconstruct_BST_with_postorder_helper(po_str, 0, len(po_str) - 1)
    return bt_temp


def reconstruct_BST_with_postorder_helper(po_str, po_start_index, po_end_index):
    if po_end_index + 1 == po_start_index:
        return None
    root = BTNode(int(po_str[po_end_index]))
    if po_start_index == po_end_index:
        return root
    i = po_end_index - 1
    while i >= po_start_index:
        if int(po_str[i]) < root.data:
            break
        i -= 1
    left_tree = reconstruct_BST_with_postorder_helper(po_str, po_start_index, i)
    right_tree = reconstruct_BST_with_postorder_helper(po_str, i + 1, po_end_index - 1)
    root.left = left_tree
    root.right = right_tree
    return root

"""
Step 0: Get to know the problem

So lets use 

             4              
     3               5      
 6       1       2       9  


in-order:  6 3 1 4 2 5 9 

pre-order: 4 3 6 1 5 2 9 

ok so we know in general for any pre order it looks like

[start: root] [start+1/x: left] [start+x+1/(start + (len(subtree) - 1)): right]

where x is less than n 

we also the general structure for an in order tree looks like

[start/x-1: left] [start + x: root] [start+x+1/(len()-1): right]

Ok we know the root is the first element of the pre-order index

If we iterate down both the in order string and hit the root on the xth iteration of the in order string 
then we know 

[0/x-1: left] [x: root] [x+1/(len()-1): right] 

is how the string is split up 

Step 1: Recursive Solution

So we know we know have [0/x-1: left] [x: root] [x+1/(len()-1): right] 

and if we can recursively split the array left and right then we can easily reconstruct the tree 

however, the way we were able to even split the in order is by knowing what the root was and that information we could
get using the pre order string so if we want to properly make a recursive call we need to consistently send the index
of the first element of the associated pre order string

so for the left tree we know the first index of the pre_order string is just pre order[start + 1]

and for the right tree we know that the first index of the pre order string is just [x + 1]

The contract for our recursive function can be given the preorder string, in order string, and starting index, and end
index return the tree 


Step 2: Pseudocode
"""


def reconstruct_BT_with_inorder_preorder(io_str, po_str):
    # make a BT
    po_str = po_str.replace(" ", "")
    io_str = io_str.replace(" ", "")
    bt_temp = BT([1])
    bt_temp.root = reconstruct_BT_with_inorder_preorder_helper(po_str, io_str, 0, len(io_str) - 1,  0)
    return bt_temp


def reconstruct_BT_with_inorder_preorder_helper(po_str, io_str, start_index, end_index, root_index):
    if end_index + 1 == start_index:
        return None
    root = BTNode(int(po_str[root_index]))
    if end_index == start_index:
        return root
    # find where the root is in the io_str
    i = start_index
    while True:
        if int(io_str[i]) == root.data:
            break
        i += 1
    left_tree = reconstruct_BT_with_inorder_preorder_helper(po_str, io_str, start_index, i - 1,  root_index + 1)
    right_tree = reconstruct_BT_with_inorder_preorder_helper(po_str, io_str, i + 1, end_index,  root_index + (i - start_index) + 1)
    root.left = left_tree
    root.right = right_tree
    return root

def reconstruct_BT_with_inorder_postorder(io_str, po_str):
    # make a BT
    po_str = po_str.replace(" ", "")
    io_str = io_str.replace(" ", "")
    bt_temp = BT([1])
    bt_temp.root = reconstruct_BT_with_inorder_postorder_helper(po_str, io_str, 0, len(io_str) - 1, len(io_str) - 1)
    return bt_temp


def reconstruct_BT_with_inorder_postorder_helper(po_str, io_str, start_index, end_index, root_index):
    if end_index + 1 == start_index:
        return None
    root = BTNode(int(po_str[root_index]))
    if end_index == start_index:
        return root
    # find where the root is in the io_str
    i = start_index
    while True:
        if int(io_str[i]) == root.data:
            break
        i += 1
    left_tree = reconstruct_BT_with_inorder_postorder_helper(po_str, io_str, start_index, i - 1,  root_index - (i - start_index) - 1)
    right_tree = reconstruct_BT_with_inorder_postorder_helper(po_str, io_str, i + 1, end_index,  root_index - 1)
    root.left = left_tree
    root.right = right_tree
    return root









bt = BST([4, 3, 5, 6, 1, 2, 9])
print(bt)
bt + 10
print(bt)

