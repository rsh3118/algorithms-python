from InterviewPrep.Trees.DataStructures.BinaryTreeNode import BTNode
from InterviewPrep.Trees.DataStructures.BinaryTree import BT


def find_node_to_node_path(bt, num):
    paths = []
    stack = [bt.root]
    get_nodes(bt.root, stack, num, paths)


def get_nodes(node, stack, num, paths):
    if node.has_left_child():
        stack.append(node.left)
        get_nodes(node.left, stack, num, paths)
        del stack[-1]
    find_paths(node, stack, num, "", paths);
    if node.has_right_child():
        stack.append(node.right)
        get_nodes(node.right, stack, num, paths)
        del stack[-1]


def find_paths(node, stack, num, path, paths):
    path += "{}".format(node)
    num -= node.data
    # check if your done
    if num == 0:
        paths.append(path)

    # try to go up
    if stack:
        node = stack[-1]
        del stack[-1]
        path += "^"
        find_paths(node, stack, num, path, paths)

    # try to go right




bintree = BT([4, 3, 5, 6, 1, 2, 9])
print(bintree)
find_node_to_node_path(bintree, 3)

