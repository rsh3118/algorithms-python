from BinaryTree import BT
from BinaryTreeNode import BTNode


class BST(BT):
    def __init__(self, array):
        BT.__init__(self, array, bst_find_spot)

    def delete(self, val):
        (parent, node, is_left, in_tree) = self.find_context(val)
        if node.has_children():
            if node.has_left_child() and node.has_right_child():
                max_child = self.get_max(node.left)
                self.delete(max_child.data)
                max_child.right = node.right
                max_child.left = node.left
                if parent is not None:
                    if is_left:
                        parent.left = max_child
                    else:
                        parent.right = max_child
                else:
                    self.root = max_child

            if (not node.has_left_child) and node.has_right_child:
                if parent is not None:
                    if is_left:
                        parent.left = node.right
                    else:
                        parent.right = node.right
                else:
                    self.root = node.right
            if node.has_left_child and (not node.has_right_child):
                if parent is not None:
                    if is_left:
                        parent.left = node.left
                    else:
                        parent.right = node.left
                else:
                    self.root = node.right
        else:
            parent.remove_child(is_left)


    def find_context(self, val):
        parent = None
        node = None
        is_left = None
        in_tree = False
        if self.root.data == val:
            node = self.root
            return parent, node, is_left, in_tree
        return self.find(self.root, val)



    def find(self, parent, val):
        val_less_than_root = val < parent.data
        if val_less_than_root and parent.has_left_child():
            if parent.left.data == val:
                return parent, parent.left, True, True
            return self.find(parent.left, val)
        if (not val_less_than_root) and parent.has_right_child():
            if parent.right.data == val:
                return parent, parent.right, False, True
            return self.find(parent.right, val)

    def get_max(self, node):
        if node.has_right_child():
            return self.get_max(node.right)
        else:
            return node


def bst_find_spot(node, element):
    if (node.data > element) and (node.left is None):
        node.left = BTNode(element)
    if (node.data < element) and (node.right is None):
        node.right = BTNode(element)
    if (node.data > element) and (node.left is not None):
        bst_find_spot(node.left, element)
    if (node.data < element) and (node.right is not None):
        bst_find_spot(node.right, element)


bst = BST([10, 6, 14, 4, 8, 12, 16])
print(bst)
bst.delete(10)
print(bst)