def swap(array, a_index, b_index):
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp


def get_parent_index(child_index):
    return int((child_index - 1) / 2)


def get_left_child_index(parent_index):
    return 2 * parent_index + 1


def get_right_child_index(parent_index):
    return 2 * parent_index + 2


def is_index_valid(array, index):
    return index < len(array)


def node_has_children(array, index):
    return (2 * index + 1) < len(array)
