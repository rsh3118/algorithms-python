from BinaryTree import BT
from TreeFunctions import swap, get_right_child_index, get_left_child_index, is_index_valid, get_parent_index, \
    node_has_children


def is_element_min_heap_element(array, parent_index, child_index):
    return array[parent_index] <= array[child_index]


def get_min_swap_index(array, element_index):
    left_child_index = get_left_child_index(element_index)
    right_child_index = get_right_child_index(element_index)
    if not is_index_valid(array, left_child_index):
        raise Exception("There are no children to swap with")
    if not is_index_valid(array, right_child_index):
        return left_child_index
    swap_index = left_child_index if array[left_child_index] <= array[
        right_child_index] else right_child_index
    return swap_index


class MinHeap:
    array = []

    def __init__(self, get_swap_index=get_min_swap_index, is_element_heap_element=is_element_min_heap_element):
        self.get_swap_index = get_swap_index
        self.is_element_heap_element = is_element_heap_element

    def insert(self, element):
        """

        :param element:
        :return: void
        """
        self.array.append(element)
        self.heapify_up()

    def heapify_up(self):
        """

        :return: void
        """
        element_index = len(self.array) - 1
        if element_index == 0:
            return
        parent_index = get_parent_index(element_index)
        heap_appropriate = self.is_element_heap_element(self.array, parent_index, element_index)
        while not heap_appropriate:
            swap(self.array, parent_index, element_index)
            heap_appropriate = self.is_element_heap_element(self.array, parent_index, element_index)
            element_index = parent_index
            parent_index = get_parent_index(parent_index)

    def heapify_down(self):
        """

        :return:
        """
        element_index = 0
        if node_has_children(self.array, element_index):
            swap_index = self.get_swap_index(self.array, element_index)
        else:
            return
        heap_appropriate = self.is_element_heap_element(self.array, element_index, swap_index)
        while not heap_appropriate:
            swap(self.array, element_index, swap_index)
            heap_appropriate = self.is_element_heap_element(self.array, element_index, swap_index)
            element_index = swap_index
            if node_has_children(self.array, element_index):
                swap_index = self.get_swap_index(self.array, element_index)
            else:
                return

    def delete_top(self):
        """

        :return: element
        """
        self.array[0] = self.array.pop(-1)
        self.heapify_down()

    def __str__(self):
        return str(BT(self.array))


def is_element_max_heap_element(array, parent_index, child_index):
    return array[parent_index] >= array[child_index]

def get_max_swap_index(array, element_index):
    left_child_index = get_left_child_index(element_index)
    right_child_index = get_right_child_index(element_index)
    if not is_index_valid(array, left_child_index):
        raise Exception("There are no children to swap with")
    if not is_index_valid(array, right_child_index):
        return left_child_index
    swap_index = left_child_index if array[left_child_index] >= array[
        right_child_index] else right_child_index
    return swap_index


class MaxHeap(MinHeap):
    def __init__(self):
        MinHeap.__init__(self, get_max_swap_index, is_element_max_heap_element)