import math

from BinaryTreeNode import BTNode


def default_find_spot(node, element):
    """
    Creates a left complete binary tree that has no inherent ordering
    :param node: root
    :param element: value of element to be added
    :return: void
    """
    queue = [node]
    while True:
        if node.left is None:
            node.left = BTNode(element)
            return
        else:
            queue.append(node.left)
        if node.right is None:
            node.right = BTNode(element)
            return
        else:
            queue.append(node.right)
        node = queue.pop(0)

class BT:
    root = None

    def __init__(self, array, insertion_function=default_find_spot):
        self.find_spot = insertion_function
        for element in array:
            self + element;

    def __add__(self, other):
        if self.root is None:
            self.root = BTNode(other)
            return
        current_node = self.root
        self.find_spot(current_node, other)

    def __str__(self):
        return self.create_formatted_tree_string()


    """
    Step 0: 

    For in order traversal we know we are printing the left than then the root then the right

    Step 1: Recursive solution

    Here the general idea is to print if you reach the last level of the tree or have already printed the left half of 
    the tree then you print yourself and get started on the right half

    Step 2: Code
    """

    def in_order(self):
        # initialize return string with tree data in order\
        io_str = ""
        io_str = self.in_order_helper(self.root, io_str)
        # return string with node data in order
        return io_str

    def in_order_helper(self, node, io_str):
        # base case is that the node is None
        if node is None:
            return io_str
        # print left subtree
        io_str = self.in_order_helper(node.left, io_str)
        # print node
        io_str += "{} ".format(node.data)
        # print right subtree
        io_str = self.in_order_helper(node.right, io_str)

        return io_str

    """
    Step 0: Get to know the Problem

    We want to print the tree pre order which means we print the root first then we print the left subtree then we want
    to print the right subtree

    Step 1: Recursive Solution

    Here the idea is to print teh root and the print the left sub tree and the right subtree 

    We can call the print the left subtree and the right subtree by recursively calling pre order

    Step 2: code

    """

    def pre_order(self):
        # initialize the return string
        po_str = ""
        # set the return string to the output of the helper
        po_str = self.pre_order_helper(self.root, po_str)
        # return the return string containing the tree data pre order
        return po_str

    def pre_order_helper(self, node, po_str):
        # base case node is None
        if node is None:
            return po_str
        # print the node
        po_str += "{} ".format(node.data)
        # print the left subtree
        po_str = self.pre_order_helper(node.left, po_str)
        # print the right subtree
        po_str = self.pre_order_helper(node.right, po_str)
        return po_str

    """
    Step 0: Get to know the problem

    Here we want to print the left subtree the right subtree and then the root 

    Step 1: Recursive Solution

    Here we want print the root then print the left and right subtrees recursivelyu

    the base case is when 

    the node is None

    Step 2: Code

    """

    def post_order(self):
        # call helper with empty string and return output
        return self.post_order_helper(self.root, "")

    def post_order_helper(self, node, po_str):
        if node is None:
            return po_str
        # add left subtree
        po_str = self.post_order_helper(node.left, po_str)
        # add right subtree
        po_str = self.post_order_helper(node.right, po_str)
        # add root
        po_str += "{} ".format(node.data)
        # return
        return po_str

    def create_formatted_tree_string(self):
        depth = 0
        index = 0
        queue = [(depth, self.root, index)]
        node_level_map = dict()
        node_level_map[depth] = [self.root.data]
        max_buffer_length = 4
        tree_depth = 0
        while len(queue) > 0:
            current_tuple = queue.pop(0)
            depth = current_tuple[0] + 1
            parent_node = current_tuple[1]
            left_child = parent_node.left
            right_child = parent_node.right
            parent_index = current_tuple[2]
            if depth not in node_level_map and (left_child is not None or right_child is not None):
                tree_depth = max(tree_depth, depth)
                node_level_map[depth] = []
            if left_child is not None:
                max_buffer_length = max(max_buffer_length, len(str(left_child.data)))
                is_right_node = False
                node_level_map = self.update_node_level_map(left_child, parent_index, depth, node_level_map,
                                                            is_right_node)
                queue = self.update_queue(left_child, parent_index, depth, queue, is_right_node)
            if right_child is not None:
                max_buffer_length = max(max_buffer_length, len(str(right_child.data)))
                is_right_node = True
                node_level_map = self.update_node_level_map(right_child, parent_index, depth, node_level_map,
                                                            is_right_node)
                queue = self.update_queue(right_child, parent_index, depth, queue, is_right_node)
        for i in range(len(node_level_map)):
            ideal_node_array_len = 2 ** i
            while len(node_level_map[i]) < ideal_node_array_len:
                node_level_map[i].append(None)
        tree_str = ""
        temp_depth = 0;
        print(node_level_map)
        while temp_depth <= tree_depth:
            tree_level_str = self.format_level(tree_depth, node_level_map[temp_depth], max_buffer_length)
            tree_str += tree_level_str
            tree_str += "\n"
            temp_depth += 1
        return tree_str

    def update_queue(self, current_node, parent_index, current_depth, queue, is_right_node):
        node_index = (2 * parent_index + 1)
        if is_right_node:
            node_index += 1
        queue.append((current_depth, current_node, node_index))
        return queue

    def update_node_level_map(self, current_node, parent_index, current_depth, node_level_map, isRightNode):
        node_index = (2 * parent_index + 1)
        if isRightNode:
            node_index += 1
        array_index = node_index - (2 ** current_depth - 1)
        node_level_map[current_depth] = self.insert_with_empty(node_level_map[current_depth], array_index,
                                                               current_node.data)
        return node_level_map

    def insert_with_empty(self, array, index, element):
        endIndex = len(array) - 1
        while endIndex < index:
            endIndex = endIndex + 1
            array.insert(endIndex, None)
        array[index] = element
        return array

    def format_level(self, tree_depth, node_level_array, buffer_length):
        level_str = ""
        num_of_lowest_level_nodes = 2 ** tree_depth
        space_for_lowest_level_nodes = num_of_lowest_level_nodes * 2 - 1
        depth_of_node_level_array = math.log(len(node_level_array)) / math.log(2)
        depth_of_each_subtree_on_level = tree_depth - depth_of_node_level_array
        num_leaves_of_each_subtree_on_level = 2 ** depth_of_each_subtree_on_level
        space_for_leaves_of_each_subtree_on_level = num_leaves_of_each_subtree_on_level * 2 - 1
        num_of_subtrees_on_level = len(node_level_array)
        """
        we always know that we need half of the space for the first subtrees elements to be to the left of the first
        subtrees root node minus the space separating it from the other half (as that space will be provided by the
        subtrees root node) so to accomplish this we can just take the lower bound of the half the space the leaves of 
        that subtree will take
        """
        level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level / 2)
        level_str += self.format_node(node_level_array.pop(0), buffer_length)
        """
        between adjacent root nodes we always need space for the right half of the left root's subtree and we need 
        space for the left half of the right root nodes subtree and we want to make sure to we do thus until we have 
        completed all adjacent nodes
        """
        while len(node_level_array) > 0:
            level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level)
            root_node_value = self.format_node(node_level_array.pop(0), buffer_length)
            level_str += root_node_value
        level_str += (" " * buffer_length) * int(space_for_leaves_of_each_subtree_on_level / 2)
        if len(level_str) != space_for_lowest_level_nodes * buffer_length:
            raise Exception(
                "The formatted binary tree level string is not as long as the lowest level string\n{} is of size {} but it should have been of size {}".format(
                    level_str, len(level_str), space_for_lowest_level_nodes * buffer_length))
        return level_str

    def format_node(self, value, buffer_length):
        if value is not None:
            node_data_str = str(value)
        else:
            node_data_str = "None"
        if len(node_data_str) > buffer_length:
            raise ValueError(
                "Insufficient Buffer Length\nNeed buffer lenght of size {} for {} but found buffer of size {}".format(
                    len(node_data_str), value, buffer_length))
        num_beg_empty_spaces = int((buffer_length - len(node_data_str)) / 2)
        node_buffer_str = " " * int((buffer_length - len(node_data_str)) / 2)
        node_buffer_str += node_data_str
        num_end_empty_spaces = buffer_length - len(node_buffer_str)
        node_buffer_str += " " * num_end_empty_spaces
        return node_buffer_str