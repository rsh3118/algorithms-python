class BTNode:
    left = None
    right = None

    def __init__(self, data):
        self.data = data

    def __add__(self, other):
        if self.left is None:
            self.left = other
        else:
            if self.right is None:
                self.right = other
            else:
                raise Exception("Node already has left and right child")

    def __str__(self):
        return str(self.data)

    def has_children(self):
        return self.has_left_child() or self.has_right_child()

    def has_left_child(self):
        if self.left is not None:
            return True
        return False

    def has_right_child(self):
        if self.right is not None:
            return True
        return False

    def remove_child(self, isLeft):
        if isLeft:
            self.remove_left_child()
        else:
            self.remove_right_child()

    def remove_left_child(self):
        self.left = None

    def remove_right_child(self):
        self.right = None