class LLNode:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __add__(self, other):
        self.next = other

    def __str__(self):
        return str(self.data)


class LL:
    def __init__(self, data):
        if not isinstance(data, list):
            element_list = [data]
        else:
            element_list = data

        previous_node = None
        for element in element_list:
            if previous_node is not None:
                current = LLNode(element)
                previous_node + current
                previous_node = current
            else:
                self.head = LLNode(element)
                previous_node = self.head

    def __str__(self):
        current_node = self.head
        ll_string = ""
        while current_node is not None:
            if current_node.next is None:
                ll_string = ll_string + str(current_node.data)
            else:
                ll_string = ll_string + str(current_node.data) + str("->")
            current_node = current_node.next
        return ll_string

    def reverse_iteratively(self):
        previous_node = None;
        current_node = self.head;
        next_node = self.head.next;
        while True:
            if next_node is not None:
                # This is the middle case
                current_node.next = previous_node
                next_next_node = next_node.next
                next_node.next = current_node
                previous_node = current_node
                current_node = next_node
                next_node = next_next_node
            if (previous_node is None) and (next_node is None):
                # This list has only one element so do nothing
                break
            if (previous_node is not None) and (next_node is None):
                # We are at the end of the linked list
                self.head = current_node
                current_node.next = previous_node
                break

    def reverse_recursively(self):
        self.reverse_recurse(self.head)

    def reverse_recurse(self, current_node):
        next_node = current_node.next
        current_node.next = None
        if next_node is None:
            self.head = current_node
            return
        self.reverse_recurse(next_node)
        next_node.next = current_node
        return







linked_list = LL([1, 2, 3, 4])
linked_list.reverse_iteratively()
print(linked_list)
linked_list.reverse_recursively()
print(linked_list)

